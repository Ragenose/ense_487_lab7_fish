#include "serial.h"


uint8_t inBuffer[BUFFER_SIZE];
uint8_t outBuffer[BUFFER_SIZE];
int inBufferCounter;
int outBufferCounter;


void open_serial(void){
	*regRCC_APB1ENR |= 0x00020000;  //Enable USART2
	*regRCC_APB2ENR |= 0x00000005;  //Enable AFIOEN bit0 and IOPAEN bit2
	*regGPIOA_CRL &= ~0xff00;  //For USART2
	*regGPIOA_CRL |= 0x4B00;	 //TX->PA2 RX->PA3
	*regUSART2_CR1 = 0x200C;  //UE, TE, RE
	*regUSART2_BRR = 36000000 / 9600;
}

void sendByte(uint8_t data){
	while((*regUSART2_SR & 0x80) == 0);
	*regUSART2_DR = data;
	delay_ms(1);
	while((*regUSART2_SR & 0x40) == 0);
}

uint8_t getByte(void){
	uint8_t data;
	while((*regUSART2_SR & 0x20) == 0);
	data = *regUSART2_DR;
	delay_ms(1);
	return data;
}

void sendString(uint8_t data[], int size){
	int i;
	for(i=0;i<size;i++){
		sendByte(data[i]);
	}
}
