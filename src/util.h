typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed           int int32_t;
typedef   signed       __int64 int64_t;

    /* exact-width unsigned integer types */
typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;
typedef unsigned       __int64 uint64_t;

#define MASK_BIT8  0x00000100
#define MASK_BIT24 0x01000000
#define MASK_BIT15 0x00008100
#define MASK_BIT31 0x80000000

void delay_ms(float num);
