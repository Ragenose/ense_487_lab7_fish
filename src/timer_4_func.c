#include "timer_4_ini.h"

void timer_4_ini(){
	* regRCC_APB2ENR |= 0x8;
	* regRCC_APB1ENR |= 0x4;
	* regGPIOB_CRL	 &= ~0xf0000000;
	* regGPIOB_CRL   |= 0xB0000000;  //set PB7 as AFIO
	
	* regTIM4_PSC     = 143;				//set PSC to 500KHz
	*	regTIM4_ARR     = 10000;			 //set ARR TO 
	* regTIM4_CR1    |= 0x81;				//set CR1 ARPE DIR AND CEN
	* regTIM4_CCER	 |= 0x10;				//set CC2E ENABLE
	* regTIM4_CCMR1  |= 0x6800;			//set OC2M AND OC2PE
	* regTIM4_CCR2		= 5000;				//% of pwm
	* regTIM4_EGR		 |=	0x1;				//set TO UG
	
}

void timer_4_pwm(int percent){
	* regTIM4_CCR2 = 100 * percent;
}