#include "stdint.h"
#include "registers.h"

extern Reg32 regTIM4_CNT;
extern Reg32 regTIM4_ARR;
extern Reg32 regTIM4_APER;
extern Reg32 regTIM4_PSC;
extern Reg32 regTIM4_SR;
extern Reg32 regTIM4_CR1;
extern Reg32 regTIM4_DIER;
extern Reg32 regTIM4_CCR1;
extern Reg32 regTIM4_CCR2;
extern Reg32 regTIM4_CCER;
extern Reg32 regTIM4_CCMR1;
extern Reg32 regTIM4_EGR;
extern Reg32 regRCC_APB1ENR;
extern Reg32 regRCC_APB2ENR;
extern Reg32 regGPIOB_CRL;

void timer_4_ini(void);
void timer_4_pwm(int);