#include "stdlib.h"
#include "timer_impl.h"


/*
generate 32 bit random number
by combining two 16 bit random number
*/
uint32_t random_32(void){
	uint32_t result = rand()%65535; //16
	result = (result << 16) + rand()%65535;
	return result;
}

/*
generate 64 bit random number
by combining two 32 bit random number
*/
uint64_t random_64(void){
	uint64_t result = random_32();
	result = (result << 32) + random_32();
	return result;
}

//adding two 32 or 64 bit random integer
uint16_t add(int length){
	int i;
	uint32_t rst;
	//two 32 bit random integers
	uint32_t num1,num2;
	//two 64 bit random integers
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	//average of 100 results
	for(i = 0;i <100;i++){
		switch(length){
			//32 bit case
			case 32:
				num1 = random_32();
				num2 = random_32();
				startTime = timer_start();
				rst=num1+num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			//64 bit case
			case 64:
				num3 = random_64();
				num4 = random_64();
				startTime = timer_start();
				rst=num3+num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		rst=0;
	}
	return sum/100;
}

uint16_t multiply(int length){
	int i;
	//two 32 bit random integers
	uint32_t num1,num2;
	//two 64 bit random integers
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(i = 0;i <100;i++){
		switch(length){
			case 32:
				num1 = random_32();
				num2 = random_32();
				startTime = timer_start();
				num1*num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			case 64:
				num3 = random_64();
				num4 = random_64();
				startTime = timer_start();
				num3*num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		
	}
	return sum/100;
}

uint16_t divide(int length){
	int i;
	//two 32 bit random integers
	uint32_t num1,num2;
	//two 64 bit random integers
	uint64_t num3,num4;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(i = 0;i <100;i++){
		switch(length){
			case 32:
				num1 = random_32();
				num2 = random_32();
			//while the dominator is 0, re-assign another element
				while(num2 == 0){
					num2 = random_32();
				}
				startTime = timer_start();
				num1/num2;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
			case 64:
				num3 = random_64();
				num4 = random_64();
				while(num4 == 0){
					num4 = random_64();
				}
				startTime = timer_start();
				num3/num4;
		
				stopTime=timer_stop(startTime);
				sum+=stopTime;
			break;
		}
		
	}
	return sum/100;
}


//function for 2 8 byte struct copy
uint16_t structure_8(){
	int i,j;
	//generate a temperate array to store elements
	struct_8 test,temp;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	//average the result of those assignments
	for(j = 0;j <100;j++){
			for(i=0;i<2;i++){
				//copy element from 1 array to another array
				test.random[i] = random_32();
			}
			startTime = timer_start();
			temp = test;
			stopTime=timer_stop(startTime);
			sum+=stopTime;
	}
	return sum/100;
}

//function for 2 128 byte struct copy
uint16_t structure_128(){
	int i,j;
	struct_128 test,temp;
	uint64_t sum=0;
	uint16_t startTime,stopTime;
	for(j = 0;j <100;j++){
			for(i=0;i<32;i++){
				test.random[i] = random_32();
			}
			startTime = timer_start();
			temp = test;
			stopTime=timer_stop(startTime);
			sum+=stopTime;
	}
	return sum/100;
}
